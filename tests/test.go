package main

import (
	// "net/http"
	// "io/ioutil"
	"fmt"
	"path/filepath"
	"os"
	"log"
)


func main() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
    if err != nil {
            log.Fatal(err)
    }
    fmt.Println(dir)
}